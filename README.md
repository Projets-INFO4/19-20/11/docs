# Week 1 (27/01/2020):
- We met with Franck Roudet, who gave us the nRF5340 cards
- We then tested the connectivity and if the cards were functionning properly.

# Week 2 (03/02/2020):
- We installed the large sum of pieces of software which were necessary for using the cards.
- We also tested the injection of code in the cards, and discovered how it was functionning by reading the documentation.
- We didn't test the Bluetooth yet as we were trying to use simpler code.

# Week 3 (10/02/2020):
- We tried to correctly implement our code, as we were not using the correct project structure and it caused bugs when compiling the code.
- We used a template provided by the constructor to solve this problem.

# Week 4 (17/02/2020):
- We made more tests, and began working on the bluetooth.
- However, the Bluetooth didn't seem to work, indeed we could't see the emmited packages, or even if any was sent.
- We made different tests (changing frequency, receiving instead of emitting) without success.

# Week 5 (02/03/2020):
- We managed to get the bluetooth working and tested the beacon application from zephyr project.
- We then programmed our own emitter, for further testing.
- We programmed a receiver to get the emitter data.

# Week 6 (09/03/2020):
- It appears that the AoA finding isn't working on our cards.
- We researched about getting the UUID of beacons (an unique identificator for each BT device).

# Week 7 (16/03/2020):
- We created a ticket on the Nordic DevZone to ask how to get the AoA.
- With the beginning of the confinement, we went to our homes, and only one of us took the Nordic cards with him.

# Week 8 (23/03/2020):
- The NordicDevs answered on our ticket that BT direction finding was not possible on our cards.
- We think it's still possible to do it, by using the RSSI which is the power of a Bluetooth emission.

# Week 9 (30/03/2020):
- Works on the calculation of the distance of the emitter based on the RSSI.
- Works on the location of the emitter based on said distances to captors.

# Week 10 (06/04/2020):
- We worked on the reception of information contained in the Bluetooth signal.
- We also nearly finished the triangulation formula, by using an intersection of circles.

# Week 11 (13/04/2020)
- The reception of information is nearly finished
- We finished making the formula for the triangulation

# Week 12 (20/04/2020)
- We made some tests with the cards we had on hand.
- We began working on the report

# Week 13 (23/04/2020)
- Report finished